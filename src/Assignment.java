import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Assignment {
    private JPanel root;
    private JButton momoButton;
    private JButton negimaButton;
    private JButton kawaButton;
    private JButton sunagimoButton;
    private JButton tsukuneButton;
    private JButton bonjiriButton;
    private JTextPane textPane2;
    private JButton checkOutButton;
    private JLabel l;
    private JLabel totalLabel;

    int totalPrice=0;
    void order(String food, int price){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " +food +"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation == 0){
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering " +food +"!"
            );
            String currentText = textPane2.getText();
            textPane2.setText(currentText + food +" " + price +"yen\n");

            totalPrice+=price;
            totalLabel.setText("Total" +(String.format("%10d",totalPrice)) +" yen");
        }
    }
    public Assignment() {
        momoButton.setIcon(new ImageIcon(this.getClass().getResource("momo.png")));
        negimaButton.setIcon(new ImageIcon(this.getClass().getResource("negima.png")));
        kawaButton.setIcon(new ImageIcon(this.getClass().getResource("kawa.png")));
        sunagimoButton.setIcon(new ImageIcon(this.getClass().getResource("sunagimo.png")));
        tsukuneButton.setIcon(new ImageIcon(this.getClass().getResource("tsukune.png")));
        bonjiriButton.setIcon(new ImageIcon(this.getClass().getResource("bonjiri.png")));

        totalLabel.setText("Total" +(String.format("%10d",totalPrice)) +" yen");
        
        momoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Momo",130);
            }
        });
        negimaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Negima",135);
            }
        });
        kawaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Kawa",170);
            }
        });
        sunagimoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sunagimo",190);
            }
        });
        tsukuneButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tsukune",150);
            }
        });
        bonjiriButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Bonjiri",195);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(confirmation == 0){
                        JOptionPane.showMessageDialog(null,
                                "Thank you. The total price is " + totalPrice + " yen."
                        );
                        totalPrice=0;
                        totalLabel.setText("Total" +(String.format("%10d",totalPrice)) +" yen");
                        textPane2.setText("");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Assignment");
        frame.setContentPane(new Assignment().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
